Documentation
=============

This a WIP.

Make it work
------------

Create and activate a virtual env :

    $ virtualenv myvenv
    $ cd myvenv
    $ . bin/activate


Clone this repository :

    $ git clone git@gitlab.com:atreal/openads.api.git
    $ cd opendads.api


Install buildout and build the stuff :

    $ pip install zc.buildout
    $ buildout


Run pyramid in waitress :

    $ bin/serve


WS are available on :

    http://0.0.0.0:6543/MY_WS


OpenAPI JSON is available at :

    http://0.0.0.0:6543/__api__


API
---

Plat'AU
~~~~~~~

GET /tasks => get task's listing basic data, filtered by state / some date
GET /task/{ID_TASK} => get all task's data, inculding files metadata
GET /task/{ID_TASK}/file/{ID_FILE} => get task's binary files
PUT /task/{ID_TASK} => update task's status

Configuration
~~~~~~~~~~~~~

In pyramid .ini file, those properties can be set :

- `openads.api.config_path` : the path to the json file
- `openads.api.credentials_storage` : storage mode for credentials in .json file. Should be one of (`hashed`, `plain`)

Users and backends are set in a .json. Use `tests/resources/config.json` as an exemple.

Tests
-----

Backend
~~~~~~~

You should have a working openADS app as backend, with those settings :

- a Collectivite MONO with id == 3
- *insee*, *departement* and *commune* correctly set in admin > parametre, associated with the previous Collectivite
- a legacy webservice user set in public_html/openads/services/.htpasswd, matching the one in tests/ressources/config.json

Credentials
~~~~~~~~~~~

You should define few env variables :

    $ export openads_password=mysecret # the openads admin user password

For remote api testing :

    $ export openadsapi_remote_url=https://api.myopenads.ltd:5080 # the url:port of the remote api

In tests/resources/config.json, passwords can be hashed :

    // password": "bnF6dmE=" == "admin"
    [...]
    // password": "ZnJwZXJn" == "secret"

Running tests
~~~~~~~~~~~~~

Tests are runned with `zope.testrunner`.

Run all tests ::

    $ ./bin/test

Run all tests of specific module ::

    $ ./bin/test -m test_dossier

Run one specific test ::

    $ ./bin/test -t test_05_create_dossier


MISC
----

Building offline cache
~~~~~~~~~~~~~~~~~~~~~~

You can build a cache allowing online buildouting ::

    $ buildout -c building-cache.cfg

A `cache.tgz` will be built, with a `download-cache` and `src` dir fully pre-loaded.
Unarchived in a fresh check-out, it allows to built opeands.api offline ::

    $ buildout -c buildout-offline.cfg



TODO
----
(Order means priority)

Currently
~~~~~~~~~

* Plat'AU tasks : correctly handle errors
* Refactor endpoint /config with a specific view from openADS
* Fully test created Dossier (check that all provided data are correctly set)
* Test /status EP
* instruction should be finalized
* DIA File type
* Protect Demandeur public entry point
* Include HTML UI for OpenAPI
* Include documentaion and descritpion links in headers (hhtp https://www.bortzmeyer.org/8631.html)
* Use pytest

 Disruption
~~~~~~~~~~

* Move to pyramid_openapi3
* Use typing and mypy