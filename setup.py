import os
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, "README.rst")) as f:
    README = f.read()


setup(
    name="openads.api",
    version="1.3.3.dev0",
    description="openADS API based on cornice",
    long_description=README,
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    keywords="web services",
    author="",
    author_email="",
    url="",
    packages=find_packages("src"),
    package_dir={"": "src"},
    namespace_packages=["openads"],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "passlib",
        "cachetools",
        "colander",
        "cornice",
        "cornice_swagger",
        "mr.developer",
        "openads.ws",
        "openmairie.ws",
        "requests",
        "waitress",
        "webtest",
        "zc.buildout",
    ],
    entry_points="""\
    [paste.app_factory]
    main=openads.api:main
    """,
    paster_plugins=["pyramid"],
)
