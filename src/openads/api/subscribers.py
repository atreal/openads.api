import copy
import os
import json
import base64
import codecs
import glob
import logging

from openads.api import PACKAGE_VERSION
from pyramid.events import ApplicationCreated
from pyramid.events import subscriber

log = logging.getLogger(__name__)


@subscriber(ApplicationCreated)
def load_config(event):
    """."""
    log.info(f"Starting openads.api {PACKAGE_VERSION}")
    log.info("Loading and parsing openads.api config")
    config_files = []
    config_path = event.app.registry.settings.get("openads.api.config_path")
    if config_path is not None:
        config_files.append(config_path)
    config_folder_path = event.app.registry.settings.get("openads.api.config_folder_path")
    if config_folder_path is not None:
        config_files += glob.glob(config_folder_path + "*.json")
    if len(config_files) == 0:
        config_files.append("/etc/openads.api/config.json")
    try:
        conf = { "backends": {}, "consumers": {} }
        for config_path in config_files:
            with open(config_path, "r") as f:
                temporary_conf = json.load(f)
                conf["backends"].update(temporary_conf["backends"])
                conf["consumers"].update(temporary_conf["consumers"])
    except IOError:
        config_path = (
            os.path.dirname(os.path.abspath(__file__)) + "/tests/resources/config.json"
        )
        with open(config_path, "r") as f:
            conf = json.load(f)
    credentials_storage = event.app.registry.settings.get(
        "openads.api.credentials_storage"
    )
    if credentials_storage is None:
        credentials_storage = event.app.registry.settings[
            "openads.api.credentials_storage"
        ] = "hashed"
    elif credentials_storage not in ("hashed", "plain"):
        raise ValueError(
            "Value for property `credentials_storage` unknown. Should be one of (`hashed`, `plain`)."
        )

    users_dict = {}
    for username, info in list(conf["consumers"].items()):

        info_updated = info.copy()
        backend = conf["backends"][info["backend"]].copy()
        if credentials_storage == "hashed":
            try:
                backend.update({"password": unbox_surprise(backend["password"])})
            except Exception:
                log.warning("Found a unhashable password for user %s. Raw entry will be used.", username)
                backend.update({"password": backend["password"]})
        else:
            backend.update({"password": backend["password"]})

        info_updated["backend"] = backend
        users_dict[username] = info_updated

    event.app.registry.settings["openads.api.config"] = {"users": users_dict}

    # We use a deepcopy() to avoid aletring settings
    anonymous_settings = copy.deepcopy(event.app.registry.settings["openads.api.config"])
    for user, values in anonymous_settings["users"].items():
        new_values = values.copy()
        new_values['backend']["password"] = "REMOVED"
        new_values["password"] = "REMOVED"
        anonymous_settings["users"][user] = new_values

    log.debug(
        "openads.api started with config : %s"
        % anonymous_settings
    )


def unbox_surprise(chain):
    """Ho ho ho, Santa Claus style."""
    return codecs.decode(base64.decodebytes(chain.encode()).decode(), "rot_13")
