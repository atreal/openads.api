"""Main entry point
"""
import pkg_resources

from pyramid.authentication import BasicAuthAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from openads.api.security import check_credentials

PACKAGE_VERSION = str(pkg_resources.get_distribution(__package__)).split(' ')[-1]


def main(global_config, **settings):
    config = Configurator(settings=settings)

    authn_policy = BasicAuthAuthenticationPolicy(check_credentials, realm=__name__)
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(ACLAuthorizationPolicy())

    config.include("cornice")
    config.include("cornice_swagger")
    config.scan("openads.api")

    return config.make_wsgi_app()
