import json

from cornice.resource import resource, view
from base64 import b64decode, b64encode
from openads.ws.platautask import PlatauTask

from openads.api.schemas.platau import (
    SearchTasksPathSchema,
    TaskPathSchema,
    PostTaskSchema,
    PutTaskSchema,
    GetFilePathSchema,
)
from openads.api.views.base import BaseRessource
from cornice.validators import (
    colander_body_validator,
    colander_path_validator,
    colander_validator,
    colander_querystring_validator,
)

from pyramid.security import Allow, Everyone, Authenticated, ALL_PERMISSIONS


def search_validator(request, **kwargs):
    if (
        "external_uid" not in request.validated
        and "external_uid_type" in request.validated
    ):
        request.errors.add(
            "path",
            "external_uid_type",
            "Providing values for external_uid_type without external_uid has no sense.",
        )
        request.errors.status = 400


def update_validator(request, **kwargs):
    if (
        "state" not in request.validated["body"]
        and "external_uid" not in request.validated["body"]
    ):
        request.errors.add(
            "body", "state|external_uid", "At least one property should be provided"
        )
        request.errors.status = 400


def create_validator(request, **kwargs):
    provided_keys = set(json.loads(request.json["json_payload"]).keys())
    mandatory_keys = {"external_uids"}

    if not mandatory_keys.issubset(provided_keys):
        request.errors.add("body", "json_payload", "Some mandatory keys are missing")
        request.errors.status = 400
        return

    if (
        request.json["typology"] == "add_piece"
        and "document_numerise" not in provided_keys
    ):
        request.errors.add("body", "json_payload", "Key `document_numerise` is missing")
        request.errors.status = 400
        return


@resource(collection_path="/platau/tasks", path="/platau/task/{task_id}")
class PlatauTasksRessource(BaseRessource):
    """."""

    def __acl__(self):
        """Global Access Control List."""
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            (Allow, Everyone, "Status : Access data"),
        ]

    @view(
        schema=SearchTasksPathSchema,
        validators=(colander_querystring_validator, search_validator),
        accept="application/json",
        permission="Platau : Search tasks",
    )
    def collection_get(self):
        """."""
        category = (
            "category" in self.request.validated
            and self.request.validated["category"]
            or None
        )
        state = (
            "state" in self.request.validated
            and self.request.validated["state"]
            or None
        )
        typology = (
            "typology" in self.request.validated
            and self.request.validated["typology"]
            or None
        )
        stream = (
            "stream" in self.request.validated
            and self.request.validated["stream"]
            or None
        )
        numero_dossier = (
            "numero_dossier" in self.request.validated
            and self.request.validated["numero_dossier"]
            or None
        )
        external_uid = (
            "external_uid" in self.request.validated
            and self.request.validated["external_uid"]
            or None
        )
        external_uid_type = (
            "external_uid_type" in self.request.validated
            and self.request.validated["external_uid_type"]
            or None
        )
        return PlatauTask(self.backend).search(
            category=category,
            state=state,
            typology=typology,
            stream=stream,
            numero_dossier=numero_dossier,
            external_uid=external_uid,
            external_uid_type=external_uid_type,
        )

    @view(
        schema=TaskPathSchema,
        validators=(colander_path_validator,),
        accept="application/json",
        permission="Platau : Read task",
    )
    def get(self):
        """."""
        return PlatauTask(self.backend).get(self.request.validated["task_id"])

    @view(
        schema=PutTaskSchema,
        validators=(colander_validator, update_validator),
        accept="application/json",
        permission="Platau : Update task",
    )
    def put(self):
        """."""
        return PlatauTask(self.backend).update(
            self.request.validated["path"]["task_id"], **self.request.validated["body"]
        )

    @view(
        schema=PostTaskSchema,
        validators=(colander_body_validator, create_validator),
        accept="application/json",
        permission="Platau : Create task",
    )
    def collection_post(self):
        """."""
        result = PlatauTask(self.backend).create(
            self.request.validated["category"],
            self.request.validated["typology"],
            self.request.validated["json_payload"],
        )
        if result:
            self.request.response.status_code = 201
            # self.request.response.text = "Task successfully created."
            return {"message": "Task successfully created."}


@resource(path="/platau/task/{task_id}/file")
class PlatauFileRessource(BaseRessource):
    """."""

    def __acl__(self):
        """Global Access Control List."""
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            (Allow, Everyone, "Status : Access data"),
        ]

    @view(
        schema=GetFilePathSchema,
        validators=(colander_path_validator,),
        accept="application/json",
        permission="Platau : Read file",
    )
    def get(self):
        """."""
        file_data = PlatauTask(self.backend).get_file(self.request.validated["task_id"])

        if file_data is None:
            self.request.errors.add(
                "path", "File not found", "There is no file attached to this task."
            )
            self.request.errors.status = 404
            return

        return {
            "files": [
                {
                    "filename": file_data["filename"],
                    "content_type": file_data["content_type"],
                    "b64_content": b64encode(file_data["content"]),
                }
            ]
        }
