from cornice.resource import resource, view
from openads.ws.dossier import Dossier
from openads.api.schemas.dossier_operations import (
    DossierDepotPossibleOperationSchema,
    DossierExisteOperationSchema,
    dossier_exists_response_schemas,
    depot_possible_response_schemas,
)
from openads.api.views.base import BaseRessource
from cornice.validators import colander_validator

from pyramid.security import Allow, Everyone, Authenticated, ALL_PERMISSIONS


# def dossier_operation_validator(request, **kwargs):
#     """Validate coherence between provided vars."""
#     if "path" not in request.validated or "body" not in request.validated:
#         return
#     if (
#         request.validated["path"]["operation"] == "depot_possible"
#         and "type_demande" not in request.validated["body"]
#     ):
#         request.errors.add(
#             "body", "type_demande", "You should provide a `type_demande`.",
#         )
#         request.errors.status = 400
#     elif (
#         request.validated["path"]["operation"] == "existe"
#         and "type_demande" in request.validated["body"]
#     ):
#         request.errors.add(
#             "body",
#             "type_demande",
#             "Providing `type_demande` value while testing for Dossier existence makes no sense",
#         )
        # request.errors.status = 400


@resource(path="/dossier/{dossier_id}/existe")
class DossierExisteRessource(BaseRessource):
    """Allow few operations on a Dossier."""

    def __acl__(self):
        """Global Access Control List."""
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            (Allow, Everyone, "Status : Access data"),
        ]

    @view(
        schema=DossierExisteOperationSchema,
        response_schemas=dossier_exists_response_schemas,
        validators=(colander_validator,),
        accept="application/json",
        permission="Dossier: ask id Dossier exists",
    )
    def get(self):
        """Check if a given Dossier exists in backend."""
        response = Dossier(self.backend).dossier_exists(
            self.request.validated["path"]["dossier_id"]
        )
        if self.request.validated["querystring"].get("response_format", "") == "publik":
            return {"err": 0, "data": response}
        else:
            return response


@resource(path="/dossier/{dossier_id}/depot_possible")
class DossierDepotPossibleRessource(BaseRessource):
    """Allow few operations on a Dossier."""

    def __acl__(self):
        """Global Access Control List."""
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            (Allow, Everyone, "Status : Access data"),
        ]

    @view(
        schema=DossierDepotPossibleOperationSchema,
        response_schemas=dossier_exists_response_schemas,
        validators=(colander_validator,),
        accept="application/json",
        permission="Dossier: ask if Depot is possible",
    )
    def post(self):
        """Check if a given Depot type is allowed on given Dossier in backend."""
        response = Dossier(self.backend).depot_possible(
            self.request.validated["path"]["dossier_id"],
            self.request.validated["body"]["type_demande"],
        )
        if self.request.validated["querystring"].get("response_format", "") == "publik":
            return {"err": 0, "data": response}
        else:
            return response
