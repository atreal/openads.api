from cornice.resource import resource, view
from openads.ws.dossier import Dossier
from openads.api.schemas.files import PiecesFullSchema, piece_response_schemas
from openads.api.views.base import BaseRessource
from cornice.validators import colander_validator
from base64 import b64decode

from pyramid.security import Allow, Everyone, Authenticated, ALL_PERMISSIONS


def piece_validator(request, **kwargs) -> None:
    # If a 111 piece_type is provided (ie. "Autre"), a description should be provided
    if not isinstance(request.json, list):
        request.errors.status = 400
        request.errors.add(
            "body", "pieces", "`pieces` property should be an array",
        )
        return
    bad_custom_pieces = [
        p["filename"]
        for p in request.json
        if p["piece_type"] == 111
        and ("custom_piece_type" not in p or not p["custom_piece_type"])
    ]
    if bad_custom_pieces != []:
        request.errors.status = 400
        request.errors.add(
            "body",
            "pieces",
            f"custom_piece_type not provided for Piece(s) {bad_custom_pieces}",
        )
        return


@resource(path="/dossier/{dossier_id}/pieces")
class PieceRessource(BaseRessource):
    """."""

    def __acl__(self):
        """Global Access Control List."""
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            # (Allow, Everyone, "Status : Access data"),
        ]

    @view(
        schema=PiecesFullSchema(),
        response_schemas=piece_response_schemas,
        validators=(colander_validator, piece_validator),
        permission="Pieces : Add Piece",
    )
    def post(self):
        """."""
        dossier = Dossier(self.backend)
        pieces = self.request.validated["body"]
        dossier_id = self.request.validated["path"]["dossier_id"]

        result = dossier.dossier_exists(dossier_id)

        if result is None or not result["existe"]:
            self.request.errors.status = 404
            self.request.errors.add(
                "path", "dossier_id", f"Dossier Number {dossier_id} does not exist.",
            )
            return

        for piece in pieces:
            piece["content"] = b64decode(piece.pop("b64_content"))

        result = dossier.add_pieces(dossier_id, pieces)

        return {"numero_dossier": dossier_id, "pieces": result}
