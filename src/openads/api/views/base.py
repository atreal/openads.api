import logging
from builtins import object
from cornice.resource import resource, view
from openads.ws.dossier import Dossier
from openads.ws.demandeur import Demandeur
from openmairie.ws.base import OMBaseObject
from openads.api.schemas.dossier import (
    DemandeurSchema,
    DossierFullSchema,
    DossierFilesFullSchema,
    DossierPathSchema,
    DossierCourrierPathSchema,
    LegacyDossierPathSchema,
)
from openads.api.schemas.base import StatusPathSchema, ConfigSchema
from cornice.service import get_services
from cornice.validators import (
    colander_validator,
    colander_path_validator,
    colander_body_validator,
)
from base64 import b64decode, b64encode

from pyramid.security import Allow, Everyone, Authenticated, ALL_PERMISSIONS

log = logging.getLogger(__name__)


class BaseRessource(object):
    """This is a base class resource.

    You should inherit from this for every entry point, in order to get a proper
    configuration ste."""

    def __init__(self, request, context=None):
        """."""
        self.request = request
        message = f"{request.method} {request.current_route_path()}"
        if request.query_string:
            message += f" {request.query_string}"
        log.info(message)

    @property
    def backend(self):
        """."""
        users = self.request.registry.settings["openads.api.config"]["users"]
        return users[self.request.authenticated_userid]["backend"]

    def __acl__(self):
        """Global Access Control List."""
        return [
            (Allow, Authenticated, ALL_PERMISSIONS),
            (Allow, Everyone, "Status : Access data"),
        ]


@resource(path="/status")
class APIStatusRessource(BaseRessource):
    """."""

    @view(permission="Status : Access data")
    def get(self):
        """."""
        return {"msg": "Running"}


@resource(path="/status/{user}")
class StatusRessource(BaseRessource):
    """."""

    @view(
        schema=StatusPathSchema,
        validators=(colander_path_validator,),
        accept="application/json",
        permission="Status : Access data",
    )
    def get(self):
        """."""
        user = self.request.validated["user"]
        users = self.request.registry.settings["openads.api.config"]["users"]
        status = []
        if user == "all":
            for user in users.values():
                status.append(
                    {
                        "url": user["backend"]["url"],
                        "status": OMBaseObject(user["backend"]).status(),
                    }
                )
        else:
            status = OMBaseObject(users[user]["backend"]).status()

        return status


@resource(path="/config/{om_param}")
class ConfigRessource(BaseRessource):
    """."""

    def __acl__(self):
        """Global Access Control List."""
        return [
            # (Allow, Authenticated, ALL_PERMISSIONS),
            (Allow, Authenticated, "Config : Access property"),
            # (Allow, Everyone, "Config : Add property"),
        ]

    @view(
        permission="Config : Access property",
        schema=ConfigSchema,
        validators=(colander_validator,),
        accept="application/json",
    )
    def get(self):
        """."""
        om_param = self.request.validated["path"]["om_param"]

        insee = self.request.validated["querystring"].get("insee", None)
        properties = OMBaseObject(self.backend).get_om_parametre(om_param, insee=insee)

        if self.request.validated["querystring"].get("response_format", "") == "publik":
            return {"err": 0, "data": properties}
        else:
            return properties

    # @view(
    #     permission="Config : Add property",
    #     schema=ConfigPathSchema,
    #     validators=(colander_path_validator,),
    #     accept="application/json",
    # )
    # def set(self):
    #     """."""
    #     property_label = self.request.validated["property_label"]
    #     property_value = OMBaseObject(self.backend).get_om_parametre(property_label)

    #     return property_value
