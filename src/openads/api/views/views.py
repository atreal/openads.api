from cornice.resource import resource, view
from openads.ws.dossier import Dossier
from openads.ws.demandeur import Demandeur
from openads.ws.instruction import Instruction
from openads.api.schemas.dossier import (
    DemandeurSchema,
    DossierCourrierPathSchema,
    DossierFilesFullSchema,
    DossierFullSchema,
    DossierPathSchema,
    LegacyDossierPathSchema,
    PostInstructionSchema,
)
from openads.api.views.base import BaseRessource
from cornice.validators import (
    colander_validator,
    colander_path_validator,
    colander_querystring_validator,
    colander_body_validator,
)
from base64 import b64decode, b64encode

from pyramid.security import Allow, Everyone, Authenticated, ALL_PERMISSIONS


@resource(collection_path="/demandeurs", path="/demandeur/{id}")
class DemandeurRessource(BaseRessource):
    """."""

    @view(accept="application/json", permission="Demandeur : Access data")
    def get(self):
        """."""
        demandeur = Demandeur(self.backend)
        data = demandeur.get(self.request.matchdict["id"])
        return data

    @view(
        schema=DemandeurSchema(),
        validators=(colander_body_validator, "validate_demandeur"),
        content_type="application/json",
        permission="Demandeur : Create demandeur",
    )
    def collection_post(self):
        """."""
        demandeur = Demandeur(self.backend)
        return demandeur.set(self.request.validated)

    def validate_demandeur(self, request, **args):
        """."""
        request.validated = request.json


@resource(
    collection_path="/dossiers", path="/dossier/{dossier_type}/{id}"
)
class DossierRessource(BaseRessource):
    """."""

    @view(
        schema=DossierPathSchema,
        validators=(colander_path_validator,),
        accept="application/json",
        permission="Dossier : Access data",
    )
    def get(self):
        """."""
        dossier = Dossier(self.backend)
        return dossier.get(self.request.validated)

    @view(
        schema=DossierFullSchema(),
        validators=(colander_validator,),
        permission="Dossier : Create dossier",
    )
    def collection_post(self):
        """."""
        dossier = Dossier(self.backend)
        data = self.request.validated["body"].copy()
        data.update(self.request.validated["path"])
        response = dossier.set(data)

        return {
            "numero_dossier": response["numero_dossier"],
            "files": [
                {
                    "filename": response["filename"],
                    "content_type": "text/plain",
                    "b64_content": b64encode(response["content"]),
                }
            ],
        }


@resource(path="/dossier/{id}/files")
class FilesRessource(BaseRessource):
    """."""

    @view(
        schema=DossierFilesFullSchema(),
        validators=(colander_validator,),
        permission="Files : Add file",
    )
    def post(self):
        """."""
        dossier = Dossier(self.backend)
        files = self.request.validated["body"]
        dossier_id = self.request.validated["path"]["id"]
        for f in files:
            # XXX - in the validator we should :
            #   - decode b64 content
            #   - check the filename and extension
            f["content"] = b64decode(f.pop("b64_content"))
            f["content_type"] = "application/pdf"

        result = dossier.add_files(dossier_id, files)

        return {"numero_dossier": dossier_id, "files": result}


@resource(path="/dossier/{id}/courrier/{courrier_type}")
class CourrierRessource(BaseRessource):
    """."""

    @view(
        schema=DossierCourrierPathSchema,
        validators=(colander_path_validator,),
        accept="application/json",
        permission="CourrierRessource : Access data",
    )
    def get(self):
        """."""
        dossier = Dossier(self.backend)
        dossier_id = self.request.validated["id"]
        courrier_type = self.request.validated["courrier_type"]
        courrier_data = dossier.get_courrier(dossier_id, courrier_type)

        if courrier_data is None:
            self.request.errors.add(
                "path", "Courrier not found", "There is no courrier of specified type"
            )
            self.request.errors.status = 404
            return

        return {
            "files": [
                {
                    "filename": courrier_data["filename"],
                    "content_type": "text/plain",
                    "b64_content": b64encode(courrier_data["content"]),
                }
            ]
        }


@resource(collection_path="/instructions", path="/instruction/{instruction_id}")
class InstructionRessource(BaseRessource):
    """."""

    @view(
        schema=PostInstructionSchema(),
        validators=(colander_body_validator,),
        permission="Instruction : create instruction",
    )
    def collection_post(self):
        """."""
        # dossier_id = self.request.validated["dossier_id"]
        # instruction = self.request.validated["instruction"]
        # event_date = "date_evenement" in self.request.validated then ["date_evenement"]

        try:
            result = Instruction(self.backend).create(**self.request.validated)
        except ValueError:
            self.request.errors.add(
                "body",
                f"Instruction {self.request.validated['instruction']} cannot be strictly identified",
                "There is more than one instruction with this label",
            )
            self.request.errors.status = 400
            return

        if result is None:
            self.request.errors.add(
                "body",
                f"Instruction {self.request.validated['instruction']} not found",
                "There is no instruction with this label",
            )
            self.request.errors.status = 404
            return

        return {
            "instruction_id": result,
        }


@resource(path="/dossier/{id}")
class LegacyDossierRessource(BaseRessource):
    """."""

    @view(
        schema=LegacyDossierPathSchema,
        validators=(colander_path_validator,),
        accept="application/json",
        permission="LegacyDossier : Access data",
    )
    def get(self):
        """."""
        dossier = Dossier(self.backend)
        return dossier.legacy_get(self.request.validated)
