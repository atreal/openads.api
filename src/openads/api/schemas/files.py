import colander

from .base import FileSchema, KOResponseSchema, DossierIdPathSchema

########################################################################################
# Piece REQUEST Schema #
########################################################################################
class PieceSchema(FileSchema):
    """."""
    piece_type = colander.SchemaNode(
        colander.Int(),
        description="Piece typology, as provided in Plat'AU",
        validator=colander.Range(1, 152),
    )

    custom_piece_type = colander.SchemaNode(
        colander.String(),
        description="Custom Piece typology, if `piece_type` set to 111",
        missing=colander.drop,
    )

    reception_date = colander.SchemaNode(
        colander.Date(),
        description="The date of reception by the organization",
        missing=colander.drop,
    )


class PiecesSchema(colander.SequenceSchema):
    """."""

    pieces = PieceSchema()


class PiecesFullSchema(colander.MappingSchema):
    """."""

    path = DossierIdPathSchema()

    body = PiecesSchema()


########################################################################################


########################################################################################
# Piece RESPONSE Schema #
########################################################################################

class PieceResponseSchema(colander.MappingSchema):
    filename = colander.SchemaNode(
        colander.String(), description="Full name of the file"
    )

    status = colander.SchemaNode(
        colander.String(),
        description="File transfert status",
        validator=colander.OneOf(("OK", "KO")),
    )


class PiecesResponseSchema(colander.SequenceSchema):
    pieces = PieceResponseSchema()


class PieceResponseSchema(colander.MappingSchema):
    dossier = DossierIdPathSchema()
    pieces = PiecesResponseSchema()


class OKResponseSchema(colander.MappingSchema):
    body = PieceResponseSchema()


# Aggregate the response schemas for get requests
piece_response_schemas = {
    "200": OKResponseSchema(
        description="Return a value when everything went all right"
    ),
    "400": KOResponseSchema(
        description="Return a message when encoutering bad formated request"
    ),
    "404": KOResponseSchema(description="Return a message when something is not found"),
}

########################################################################################
