import colander

from .base import (
    AdresseSchema,
    ReferenceCadastraleSchema,
    FilesSchema,
    DemandeurSchema,
    MandataireSchema,
    EngagementSchema
)


class DemandeursSchema(colander.SequenceSchema):
    """."""

    demandeur = DemandeurSchema()


class MandatairesSchema(colander.SequenceSchema):
    """."""

    mandataire = MandataireSchema()


class ReferencesCadastralesSchema(colander.SequenceSchema):
    """."""

    reference_castrale = ReferenceCadastraleSchema()


class TerrainSchema(AdresseSchema):
    """."""

    references_cadastrales = ReferencesCadastralesSchema()


class DossierSchema(colander.MappingSchema):

    """."""

    collectivite = colander.SchemaNode(colander.Int())
    type_detaille = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(("DIA", "CUa", "CUb", "DP", "DPS", "PC", "PI")),
        missing=colander.drop,
    )
    terrain = TerrainSchema()
    demandeurs = DemandeursSchema()
    mandataires = MandatairesSchema(missing=colander.drop)
    engagement = EngagementSchema(missing=colander.drop)


class DossierPathSchema(colander.MappingSchema):
    """."""

    id = colander.SchemaNode(
        colander.String(),
        validator=colander.Regex(
            "^(cu|CU|dp|DP|pc|PC|pa|PA|pd|PD|at|AT|dia|DIA)\d{3}\d{3}\d{2}(0|[a-zA-Z])\d{4}$"
        ),
        missing=colander.drop,
    )


class DossierFullSchema(colander.MappingSchema):
    """."""

    body = DossierSchema()

    path = DossierPathSchema()


class DossierFilesFullSchema(colander.MappingSchema):
    """."""

    body = FilesSchema()

    path = DossierPathSchema()


class DossierCourrierPathSchema(DossierPathSchema):
    """."""

    courrier_type = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(
            (
                # DIA
                "dia_renonciation_preempter",
                "dia_souhait_preempter",
                "dia_irrecevabilite",
                # CU
                "CUa",
                "CUb - ACCORD",
                # DP
                "decision_nom_opposition_sr",
                "decision_opposition_DP",
                # PC
                "arrete_ss_reserves",
                "arrete_avec_reserves",
                "arrete_refus",
            )
        ),
    )


class InstructionPathSchema(colander.MappingSchema):

    evenement_id = colander.SchemaNode(colander.String(),)


class PostInstructionSchema(colander.MappingSchema):

    dossier_id = colander.SchemaNode(colander.String(),)

    evenement = colander.SchemaNode(colander.String(),)

    lettre_type = colander.SchemaNode(colander.Boolean(), missing=colander.drop,)

    date_evenement = colander.SchemaNode(colander.String(), missing=colander.drop,)


class LegacyDossierPathSchema(colander.MappingSchema):
    """."""

    id = colander.SchemaNode(
        colander.String(),
        validator=colander.Regex(
            "^(cu|CU|dp|DP|pc|PC|pa|PA|pd|PD|at|AT|dia|DIA)\d{3}\d{3}\d{2}([0-9a-zA-Z]{1,2})\d{4}((M|T|ANNUL|DOC|DAACT|PRO)(0[1-9]|[1-9][0-9])|P0)?$"
        ),
        missing=colander.drop,
    )
