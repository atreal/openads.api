import colander

from .base import (
    KOResponseSchema,
    DossierIdPathSchema,
)

########################################################################################
# DossierOperation REQUEST schema #
########################################################################################
class DossierOperationQuerystringSchema(colander.MappingSchema):
    """."""

    response_format = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(("publik",)),
        missing=colander.drop,
        description="Force a specific response format. Ex : `publik`",
    )


class DossierDepotPossibleOperationBodySchema(colander.MappingSchema):
    """."""

    type_demande = colander.SchemaNode(
        colander.String(),
        missing=colander.drop,
        description="The Demande type code, as available in openADS. Ex: `DPS` if you want to know if a `Depot de Pieces Supplementaires` is allowed.",
    )


class DossierExisteOperationSchema(colander.MappingSchema):
    """."""

    path = DossierIdPathSchema()

    querystring = DossierOperationQuerystringSchema()


class DossierDepotPossibleOperationSchema(colander.MappingSchema):
    """."""

    path = DossierIdPathSchema()

    body = DossierDepotPossibleOperationBodySchema()

    querystring = DossierOperationQuerystringSchema()


########################################################################################
# DossierOperation RESPONSE Schema #
########################################################################################
class DossierExistsResponseSchema(colander.MappingSchema):
    existe = colander.SchemaNode(
        colander.Boolean(),
        description="When `operation` is `existe`, a boolean revealing if the Dossier exists. Not provided for any other operation.",
        missing=colander.drop,
    )


class DepotPossibleResponseSchema(colander.MappingSchema):
    depot_possible = colander.SchemaNode(
        colander.Boolean(),
        description="When `operation` is `depot_possible`, a boolean revealing if the Dossier exists. Not provided for any other operation.",
        missing=colander.drop,
    )


class DossierExistsOKResponseSchema(colander.MappingSchema):
    body = DossierExistsResponseSchema()


class DepotPossibleOKResponseSchema(colander.MappingSchema):
    body = DepotPossibleResponseSchema()


# Aggregate the response schemas for get requests
dossier_exists_response_schemas = {
    "200": DossierExistsOKResponseSchema(
        description="Return a value when everything went all right"
    ),
    "400": KOResponseSchema(
        description="Return a message when encoutering bad formated request"
    ),
    "404": KOResponseSchema(description="Return a message when something is not found"),
}
depot_possible_response_schemas = {
    "200": DepotPossibleOKResponseSchema(
        description="Return a value when everything went all right"
    ),
    "400": KOResponseSchema(
        description="Return a message when encoutering bad formated request"
    ),
    "404": KOResponseSchema(description="Return a message when something is not found"),
}
########################################################################################
