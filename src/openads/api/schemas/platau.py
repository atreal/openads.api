import colander
import json


def json_valid(value):
    """."""
    try:
        json.loads(value)
    except json.JSONDecodeError:
        return False
    return True


class SearchTasksPathSchema(colander.MappingSchema):
    """."""

    category = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(
            ("platau", "portal", "email")
        ),
        missing=colander.drop,
    )
    state = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(
            ("draft", "new", "pending", "done", "archived", "error", "debug", "canceled")
        ),
        missing=colander.drop,
    )
    typology = colander.SchemaNode(colander.String(), missing=colander.drop,)
    stream = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(("input", "output")),
        missing=colander.drop,
    )
    numero_dossier = colander.SchemaNode(colander.String(), missing=colander.drop,)
    external_uid = colander.SchemaNode(colander.String(), missing=colander.drop,)
    external_uid_type = colander.SchemaNode(colander.String(), missing=colander.drop,)


class TaskPathSchema(colander.MappingSchema):
    """."""

    task_id = colander.SchemaNode(colander.String())


class PutTaskBodySchema(colander.MappingSchema):
    """."""

    state = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(
            ("draft", "canceled", "new", "pending", "done", "archived", "error", "debug")
        ),
        missing=colander.drop,
    )
    external_uid = colander.SchemaNode(colander.String(), missing=colander.drop,)


class PutTaskSchema(colander.MappingSchema):
    """."""

    path = TaskPathSchema()

    body = PutTaskBodySchema()


class PostTaskSchema(colander.MappingSchema):
    """."""

    category = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(
            ("platau", "portal", "email")
        ),
    )
    typology = colander.SchemaNode(
        colander.String(),
        validator=colander.OneOf(
            (
                "create_DI_for_consultation",
                "add_piece",
                "pec_metier_consultation",
                "avis_consultation",
                "create_message",
                "create_DI"
            )
        ),
    )
    json_payload = colander.SchemaNode(
        colander.String(),
        validator=colander.Function(json_valid, "Provided JSON is invalid."),
    )


class GetFilePathSchema(colander.MappingSchema):
    """."""

    task_id = colander.SchemaNode(colander.String())
