# -*- coding: utf-8 -*-


from .base_testcase import BaseTestCase, dossier_payload


class DossierOperationsTestCase(BaseTestCase):
    """."""

    def test_dossier_existe(self):
        """."""
        self.app.authorization = ("Basic", ("aubagne", "secret"))
        """."""
        # Let's try to get non existant Dossier
        response = self.app.get("/dossier/PC0130559999999/existe", status=200)
        # Dossier should not exist
        self.assertFalse(response.json["existe"])

        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)
        # Get the numero
        numero_dossier = response.json["numero_dossier"]
        # And check if it exists
        response = self.app.get(f"/dossier/{numero_dossier}/existe", status=200)
        #
        self.assertIn("existe", response.json.keys())
        # Dossier should exists
        self.assertTrue(response.json["existe"])

    def test_dossier_depot_possible(self):
        """."""
        self.app.authorization = ("Basic", ("aubagne", "secret"))
        """."""
        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)
        # Get the numero
        numero_dossier = response.json["numero_dossier"]

        # And check if dummy operation doesn't exist
        data = {"type_demande": "PLOP"}
        response = self.app.post_json(
            f"/dossier/{numero_dossier}/depot_possible", data, status=200
        )
        # Response should match
        self.assertIn("depot_possible", response.json.keys())
        # Demande should not be possible
        self.assertFalse(response.json["depot_possible"])

        # Then we check if a always-available operation exists
        data = {"type_demande": "dps"}
        response = self.app.post_json(
            f"/dossier/{numero_dossier}/depot_possible", data, status=200
        )
        # Response should match
        self.assertIn("depot_possible", response.json.keys())
        # Demande should not be possible
        self.assertTrue(response.json["depot_possible"])
