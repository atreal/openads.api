# -*- coding: utf-8 -*-

from webtest import TestApp
import os
import requests
import unittest
import simplejson as json
import subprocess
import tempfile

from .base_testcase import BaseTestCase, drop_linebreak, get_file_data, dossier_payload


class DemandeurTestCase(BaseTestCase):
    """."""

    def test_create_petitionnaire(self):
        """."""
        payload = {
            "collectivite": 3,
            "type_personne": "particulier",
            "typologie": "petitionnaire",
            "nom": "Dupond",
            "prenom": "Daniel",
            "adresse": {
                "numero_voie": "22",
                "nom_voie": "Boulevard Neuf",
                "lieu_dit": "Les Borels",
                "code_postal": 13015,
                "localite": "Marseille",
            },
            "coordonnees": {"email": "d.dupond@blackmail.com"},
        }

        response = self.app.post_json("/demandeurs", payload, status=200)

        id_demandeur = response.json["id"]
        response = self.app.get("/demandeur/%s" % id_demandeur, status=200)

        # XXX - add some assertion
