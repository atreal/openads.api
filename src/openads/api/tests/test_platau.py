# -*- coding: utf-8 -*-

import os
import json

from base64 import b64decode, b64encode
from datetime import date
from .base_testcase import BaseTestCase, get_file_data, dossier_payload

creation_DI = {
    "architecte": None,
    "demandeur": None,
    "donnees_techniques": None,
    "dossier": None,
    "dossier_parcelle": None,
    "external_uid": None,
    "task": None,
}


class PlatauTestCase(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        # self.app = TestApp(main({}))
        # self.app.authorization = ("Basic", ("new_orleans", "secret"))
        super().setUp()
        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)

        # And get the numero
        self.numero_dossier = response.json["numero_dossier"]

    def test_01_search_tasks(self):
        """."""
        # Let's search for tasks
        response = self.app.get("/platau/tasks", status=200)

        # we should have some tasks regarding this Dossier
        numero_dossier_exists = False
        for task in response.json:
            if self.numero_dossier == task["dossier"]:
                numero_dossier_exists = True
                break
        self.assertTrue(numero_dossier_exists)

        # Let's search for some typologie filtered tasks
        response = self.app.get("/platau/tasks?typology=creation_DA", status=200)

        # we should have ONE task regarding this Dossier
        matching_tasks = 0
        for task in response.json:
            if self.numero_dossier == task["dossier"]:
                matching_tasks += 1

        self.assertEquals(matching_tasks, 1)

        # Let's search for some specific tasks; based on numero_dossier
        response = self.app.get(
            "/platau/tasks?numero_dossier={}".format(self.numero_dossier), status=200
        )
        # we should have ONE task regarding this Dossier
        for task in response.json:
            self.assertEquals(self.numero_dossier, task["dossier"])

    def test_02_get_task(self):
        """."""
        # Let's get our task
        response = self.app.get(
            f"/platau/tasks?typology=creation_DA&numero_dossier={self.numero_dossier}",
            status=200,
        )

        # import ipdb;ipdb.set_trace()
        task_id = response.json[0]["task"]
        response = self.app.get(f"/platau/task/{task_id}", status=200)

        # data should should mention the right task
        self.assertEquals(response.json["task"]["task"], task_id)

        # Data should be about the right Dossier
        self.assertEquals(response.json["task"]["dossier"], self.numero_dossier)

        # payload should be about one and only task
        # Assuming JSON keys are is {"task"}
        self.assertTrue(isinstance(response.json, dict))
        self.assertEqual(len(response.json), 5)
        self.assertEqual(
            set(response.json.keys()),
            set(
                [
                    "task",
                    "dossier_autorisation",
                    "donnees_techniques",
                    "dossier_autorisation_parcelle",
                    "external_uids",
                ]
            ),
        )
        print(response.json.keys())

    def test_03_update_task(self):
        """."""
        # Let's get our task
        response = self.app.get(
            "/platau/tasks?typology={}&numero_dossier={}".format(
                "creation_DI", self.numero_dossier
            ),
            status=200,
        )
        self.assertEquals(response.json[0]["state"], "new")

        task_id = response.json[0]["task"]
        response = self.app.put(
            "/platau/task/{task_id}".format(task_id=task_id),
            {"state": "done", "external_uid": "111-eee-fff"},
            status=200,
        )
        response = self.app.get(
            "/platau/task/{task_id}".format(task_id=task_id), status=200
        )
        self.assertEquals(response.json["task"]["state"], "done")
        self.assertEquals(response.json["external_uids"]["dossier"], "111-eee-fff")

    def _test_04_get_file(self):
        """WIP - XXX"""
        task_id = "1"
        file_id = "kljlk"
        response = self.app.get(
            "/platau/task/{task_id}/file/{file_id}".format(
                task_id=task_id, file_id=file_id
            ),
            status=200,
        )

    def test_05_create_task(self):
        """."""
        resource_path = os.path.dirname(os.path.abspath(__file__)) + "/resources"
        with open(f"{resource_path}/create_DI_for_consultation.json") as f:
            creation_DI = json.load(f)

        # Let's get create our task
        response = self.app.post_json(
            "/platau/tasks",
            {"typology": "create_DI_for_consultation", "json_payload": json.dumps(creation_DI)},
            status=201,
        )
