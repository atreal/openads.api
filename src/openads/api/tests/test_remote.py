# -*- coding: utf-8 -*-

from __future__ import print_function
import json
import os
import requests
import unittest

from .base_testcase import (
    get_file_data,
    dossier_payload,
    REMOTE_URL,
    COLLECTIVITE,
    AUTH,
    LEGACY_AUTH,
)


class RemoteDossierTestCase(unittest.TestCase):
    """This is a remote test case.

    Goal is to easily validate a remote deployment of openads.api.
    We use straight requests library.
    """

    types_dossier = {
        "DIA": ["DIA"],
        "CU": ["CUa", "CUb"],
        "DP": ["DP", "DPS"],
        "PC": ["PC", "PI"],
    }

    def setUp(self):
        """."""
        self.url = REMOTE_URL
        self.auth = tuple(AUTH)
        self.legacy_auth = tuple(LEGACY_AUTH)
        self.dossier_payload = dossier_payload.copy()
        self.dossier_payload["collectivite"] = COLLECTIVITE

    def test_create_demandeur_remote(self):
        """."""
        if self.url is None:
            # remote url is not defined, we assume we doesn't to run the tests
            return

        payload = {
            "collectivite": COLLECTIVITE,
            "type_personne": "particulier",
            "typologie": "petitionnaire",
            "nom": "Dupond",
            "prenom": "Daniel",
            "adresse": {
                "numero_voie": "22",
                "nom_voie": "Boulevard Neuf",
                "lieu_dit": "Les Borels",
                "code_postal": 13015,
                "localite": "Marseille",
            },
            "coordonnees": {"email": "d.dupond@blackmail.com"},
        }

        created = requests.post(
            "%s/demandeurs" % self.url, data=json.dumps(payload), auth=self.auth
        )
        self.assertTrue(created.ok)
        print(created.json())

        id_demandeur = created.json()["id"]

        read = requests.get(
            "%s/demandeur/%s" % (self.url, id_demandeur), auth=self.auth
        )

        self.assertTrue("Daniel" in list(read.json().values()))

    def test_create_dossier_remote(self):
        """."""
        if self.url is None:
            # remote url is not defined, we assume we doesn't to run the tests
            return

        for type_dossier, types_detaille in list(self.types_dossier.items()):

            for type_detaille in types_detaille:

                self.dossier_payload.update({"type_detaille": type_detaille})

                # Let's create a simple dossier
                created = requests.post(
                    "%s/dossiers/%s" % (self.url, type_dossier),
                    data=json.dumps(self.dossier_payload),
                    auth=self.auth,
                )

                print(created.json())

                # And get the numero
                numero_dossier = created.json()["numero_dossier"]

                # Now we should be able to get some data
                read = requests.get(
                    "%s/dossier/%s/%s" % (self.url, type_dossier, numero_dossier),
                    auth=self.auth,
                )

                print(read.json())

                # Then prep some nice PDF
                pdf_dir = os.path.dirname(os.path.abspath(__file__)) + "/resources"
                dia_b64 = get_file_data("%s/DIA_cerfa_10072-02.pdf" % pdf_dir)
                plan_b64 = get_file_data("%s/plancadastral.pdf" % pdf_dir)

                # And the right payload
                payload = [
                    {
                        "filename": "DIA_cerfa_10072-02.pdf",
                        "content_type": "text/plain",
                        "b64_content": dia_b64,
                        "file_type": "CERFA",
                    },
                    {
                        "filename": "plancadastral.pdf",
                        "content_type": "text/plain",
                        "b64_content": plan_b64,
                        "file_type": "plan",
                    },
                ]

                # Let's add some file !
                response = requests.post(
                    "%s/dossier/%s/%s/files" % (self.url, type_dossier, numero_dossier),
                    data=json.dumps(payload),
                    auth=self.auth,
                )

                print(response.json())

    def test_get_legacy_dossier_remote(self):
        """."""
        if self.url is None:
            # remote url is not defined, we assume we doesn't to run the tests
            return

        # Let's create a simple dossier
        created = requests.post(
            "%s/dossiers/DIA" % self.url,
            data=json.dumps(self.dossier_payload),
            auth=self.auth,
        )

        # And get the numero
        numero_dossier = created.json()["numero_dossier"]

        # Let's try to get some info, the old way
        response = requests.get(
            "%s/dossier/%s" % (self.url, numero_dossier), auth=self.legacy_auth
        )

        self.assertTrue(response.ok)

        data = response.json()

        # legacy WS is respected
        expected_keys = set(
            [
                "collectivite",
                "date_decision",
                "date_depot_initial",
                "date_limite_instruction",
                "division",
                "donnees_techniques",
                "dossier_autorisation",
                "dossier_autorisation_type",
                "dossier_autorisation_type_detaille",
                "dossier_instruction",
                "enjeu_erp",
                "enjeu_urbanisme",
                "etat_dossier",
                "instructeur",
                "petitionnaire_principal",
                "references_cadastrales",
                "statut_dossier",
                "terrain_adresse_bp",
                "terrain_adresse_cedex",
                "terrain_adresse_code_postal",
                "terrain_adresse_lieu_dit",
                "terrain_adresse_localite",
                "terrain_adresse_voie",
                "terrain_adresse_voie_numero",
                "terrain_superficie",
            ]
        )
        self.assertEqual(set(data), expected_keys)

        # Is this the right dossier ?
        self.assertEqual(data["dossier_instruction"], numero_dossier)

    def test_add_pieces_remote(self):
        """."""
        # # Let's create a simple dossier
        # response = requests.post(
        #     "%s/dossiers" % self.url,
        #     data=json.dumps(self.dossier_payload),
        #     auth=self.auth,
        #     verify=False
        # )

        # print(response.json())

        # # And get the numero
        # numero_dossier = response.json["numero_dossier"]

        numero_dossier = "PC03103322V0064"

        # Then prep some nice PDF
        pdf_dir = os.path.dirname(os.path.abspath(__file__)) + "/resources"
        dia_b64 = get_file_data("%s/DIA_cerfa_10072-02.pdf" % pdf_dir)
        plan_b64 = get_file_data("%s/plancadastral.pdf" % pdf_dir)

        # And the right payload
        payload = [
            {
                "filename": "DIA_cerfa_10072-02.pdf",
                "content_type": "application/pdf",
                "b64_content": dia_b64,
                "piece_type": 1,
            },
            {
                "filename": "plancadastral.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 5,
            },
        ]

        # Let's add some file !
        response = requests.post(
            f"{self.url}/dossier/{numero_dossier}/pieces",
            data=json.dumps(payload),
            auth=self.auth,
            verify=False,
        )

        pieces_created = response.json()["pieces"]
        for piece in pieces_created:
            self.assertEqual(piece["status"], "OK")
        payload_filenames = [p["filename"] for p in payload]
        for item in payload:
            self.assertTrue(item["filename"] in payload_filenames)

        # Let's try the special 111 demande type usecase
        # And the right payload
        payload = [
            {
                "filename": "bad_typing.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 111,
            },
        ]
        # Let's add some file !
        # This should break with a 400
        response = requests.post(
            f"{self.url}/dossier/{numero_dossier}/pieces",
            data=json.dumps(payload),
            auth=self.auth,
            verify=False,
        )

        payload = [
            {
                "filename": "good_typing.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 111,
                "custom_piece_type": "CU précédent",
            },
        ]
        # Let's add some file !
        response = requests.post(
            f"{self.url}/dossier/{numero_dossier}/pieces",
            data=json.dumps(payload),
            auth=self.auth,
            verify=False,
        )
        pieces_created = response.json()["pieces"]
        self.assertEqual(len(pieces_created), 1)

        numero_dossier = "IM_A_DUMMY_NUMBER"
         # And the right payload
        payload = [
            {
                "filename": "DIA_cerfa_10072-02.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 1,
            },
        ]
        # Let's add some file !
        response = requests.post(
            f"{self.url}/dossier/{numero_dossier}/pieces",
            data=json.dumps(payload),
            auth=self.auth,
            verify=False,
        )
        self.assertTrue(response.status_code, 400)

        numero_dossier = "PC03103322Z9999"
        # Let's add some file !
        response = requests.post(
            f"{self.url}/dossier/{numero_dossier}/pieces",
            data=json.dumps(payload),
            auth=self.auth,
            verify=False,
        )
        self.assertTrue(response.status_code, 500)
