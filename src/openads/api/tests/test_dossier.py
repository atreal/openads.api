# -*- coding: utf-8 -*-

import os

from base64 import b64decode
from datetime import date
from .base_testcase import BaseTestCase, get_file_data, dossier_payload


class DossierTestCase(BaseTestCase):
    """."""

    def test_authentication(self):
        """."""
        # We reset authorization, we should have a 401 Unauthorized
        self.app.authorization = None
        response = self.app.get("/dossier/PC0130551909999", status=401)

        # We provide unkown credentials, we should have a 401 Unauthorized
        self.app.authorization = ("Basic", ("mickey", "dingo"))
        response = self.app.get("/dossier/PC0130551909999", status=401)

        # We provide nice crendetials, but an unknown Dossier, we should have a 400
        self.app.authorization = ("Basic", ("new_orleans", "secret"))
        response = self.app.get("/dossier/PC013055190XXXX", status=400)

    def test_create_dossier(self):
        """."""
        payload = {
            "collectivite": 3,
            "type_detaille": "PC",
            "terrain": {
                "numero_voie": "20",
                "nom_voie": "boulevard Neuf",
                "lieu_dit": "",
                "code_postal": 13015,
                "localite": "Marseille",
                "references_cadastrales": [
                    {"prefixe": "898", "section": "AH", "numero": "0015"},
                    {"prefixe": "898", "section": "H", "numero": "0014"},
                    {"prefixe": "898", "section": "H", "numero": "16"},
                ],
            },
            "demandeurs": [
                {
                    "type_personne": "particulier",
                    "typologie": "petitionnaire",
                    "nom": "Durand",
                    "prenom": "Jean",
                    "adresse": {
                        "numero_voie": "22",
                        "nom_voie": "Boulevard Neuf",
                        "lieu_dit": "Les Borels",
                        "code_postal": 13015,
                        "localite": "Marseille",
                    },
                    "coordonnees": {"email": "j.durand@blackmail.com"},
                },
                {
                    "type_personne": "particulier",
                    "typologie": "petitionnaire",
                    "nom": "Pierre",
                    "prenom": "Martin",
                    "adresse": {
                        "numero_voie": "28",
                        "nom_voie": "Boulevard Liberté",
                        "lieu_dit": "L'Estaque'",
                        "code_postal": 13016,
                        "localite": "Marseille",
                    },
                    "coordonnees": {"email": "p.martin@blackmail.com"},
                },
            ],
            "mandataires": [
                {
                    "type_personne": "personne_morale",
                    "typologie": "delegataire",
                    "nom": "JACQUES",
                    "prenom": "Pierre",
                    "denomination": "Office notarial",
                    "raison_sociale": "Etude Pierre JACQUES",
                    "adresse": {
                        "numero_voie": "45",
                        "nom_voie": "Boulevard Michel",
                        "lieu_dit": "L'Estaque",
                        "code_postal": 13016,
                        "localite": "Marseille",
                    },
                    "coordonnees": {"email": "p.jacques@blackmail.com"},
                }
            ],
        }

        response = self.app.post_json("/dossiers", payload, status=200)

        # We check the response structure
        self.assertTrue('numero_dossier' in list(response.json.keys()))
        self.assertTrue('files' in list(response.json.keys()))

        # We should have only one file
        self.assertEqual(len(response.json["files"]), 1)

        # File structure should be valid
        recepisse = response.json["files"][0]
        self.assertEqual(
            set(recepisse.keys()), set(["filename", "b64_content", "content_type"])
        )

        # We should find numero de dossier in the filename
        numero_dossier = response.json["numero_dossier"]
        self.assertTrue(numero_dossier in recepisse["filename"])

        # File should not be empty
        self.assertTrue(len(recepisse["b64_content"]) != 0)

        # Is content a valid base64 ?
        content_is_ok = True
        try:
            b64decode(recepisse["b64_content"])
        except TypeError:
            content_is_ok = False
        self.assertTrue(content_is_ok)

    def test_add_files(self):
        """."""
        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)

        # And get the numero
        numero_dossier = response.json["numero_dossier"]

        # Then prep some nice PDF
        pdf_dir = os.path.dirname(os.path.abspath(__file__)) + "/resources"
        dia_b64 = get_file_data("%s/DIA_cerfa_10072-02.pdf" % pdf_dir)
        plan_b64 = get_file_data("%s/plancadastral.pdf" % pdf_dir)

        # And the right payload
        payload = [
            {
                "filename": "DIA_cerfa_10072-02.pdf",
                "content_type": "text/plain",
                "b64_content": dia_b64,
                "file_type": "CERFA",
            },
            {
                "filename": "plancadastral.pdf",
                "content_type": "text/plain",
                "b64_content": plan_b64,
                "file_type": "plan",
            },
        ]

        # Let's add some file !
        response = self.app.post_json(
            "/dossier/DIA/%s/files" % numero_dossier, payload, status=200
        )

        files_created = [f["filename"] for f in response.json["files"]]
        for item in payload:
            self.assertTrue(item["filename"] in files_created)

    def test_read_dossier(self):
        """."""
        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)

        # And get the numero
        numero_dossier = response.json["numero_dossier"]

        # Now we should be able to get some data
        response = self.app.get("/dossier/DIA/%s" % numero_dossier, status=200)
        json = response.json

        # Keys should be the ones expected
        expected_keys = set(
            [
                "etat",
                "date_depot",
                "date_limite_instruction",
                "date_decision",
                "decision",
            ]
        )
        self.assertEqual(set(json.keys()), expected_keys)

        # Date de depot should be today
        today = date.today().strftime("%d/%m/%Y")
        self.assertEqual(json["date_depot"], today)

    def test_get_courrier(self):
        """."""
        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)

        # And get the numero
        numero_dossier = response.json["numero_dossier"]

        # Let's try to get an inexisteant courrier
        response = self.app.get(
            "/dossier/%s/courrier/%s"
            % (numero_dossier, "dia_renonciation_preempter"),
            status=404,
        )

        # Let's add a non prememption decision and finalize it
        self.add_instruction(numero_dossier, 552, finalize=True)

        response = self.app.get(
            "/dossier/%s/courrier/%s"
            % (numero_dossier, "dia_renonciation_preempter"),
            status=200,
        )

        # We check the response structure
        self.assertTrue('files' in list(response.json.keys()))

        # We should have only one file
        self.assertEqual(len(response.json["files"]), 1)

        # File structure should be valid
        courrier = response.json["files"][0]
        self.assertEqual(
            set(courrier.keys()), set(["filename", "b64_content", "content_type"])
        )

        # File should not be empty
        self.assertTrue(len(courrier["b64_content"]) != 0)

        # Is content a valid base64 ?
        content_is_ok = True
        try:
            b64decode(courrier["b64_content"])
        except TypeError:
            content_is_ok = False
        self.assertTrue(content_is_ok)

    def test_get_legacy_dossier(self):
        """."""
        # Let's create a simple dossier
        self.app.authorization = ("Basic", ("new_orleans", "secret"))
        response = self.app.post_json("/dossiers", dossier_payload, status=200)

        # And get the numero
        numero_dossier = response.json["numero_dossier"]

        # Let's try to get some info, the old way
        self.app.authorization = ("Basic", ("new_orleans_sig", "secret"))
        response = self.app.get("/dossier/%s" % numero_dossier, status=200)
        data = response.json

        # legacy WS is respected
        expected_keys = set(
            [
                u"collectivite",
                u"date_decision",
                u"date_depot_initial",
                u"date_limite_instruction",
                u"division",
                u"donnees_techniques",
                u"dossier_autorisation",
                u"dossier_autorisation_type",
                u"dossier_autorisation_type_detaille",
                u"dossier_instruction",
                u"enjeu_erp",
                u"enjeu_urbanisme",
                u"etat_dossier",
                u"instructeur",
                u"petitionnaire_principal",
                u"references_cadastrales",
                u"statut_dossier",
                u"terrain_adresse_bp",
                u"terrain_adresse_cedex",
                u"terrain_adresse_code_postal",
                u"terrain_adresse_lieu_dit",
                u"terrain_adresse_localite",
                u"terrain_adresse_voie",
                u"terrain_adresse_voie_numero",
                u"terrain_superficie",
            ]
        )
        self.assertEqual(set(data), expected_keys)

        # Is this the right dossier ?
        self.assertEqual(data["dossier_instruction"], numero_dossier)
