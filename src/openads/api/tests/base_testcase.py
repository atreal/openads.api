# -*- coding: utf-8 -*-

from webtest import TestApp
import json
import os
import unittest

from base64 import b64decode, b64encode
from datetime import date
from openads.api import main
from openmairie.ws.base import OMBaseObject
from openads.api.subscribers import unbox_surprise

from pathlib import Path
from pyramid.paster import get_app

REMOTE_URL = os.getenv("openadsapi_remote_url")
COLLECTIVITE = os.getenv("openadsapi_collectivite", 3)
COLLECTIVITE_INSEE = os.getenv("openadsapi_collectivite_insee", "13005")
AUTH = os.getenv("openadsapi_auth", "new_orleans:secret").split(":")
LEGACY_AUTH = os.getenv("openadsapi_auth", "new_orleans_sig:secret").split(":")


dossier_payload = {
    "collectivite": 2,
    "type_detaille": "PC",
    "terrain": {
        "numero_voie": "20",
        "nom_voie": "boulevard Neuf",
        "lieu_dit": "",
        "code_postal": 13015,
        "localite": "Marseille",
        "references_cadastrales": [
            {"prefixe": "898", "section": "H", "numero": "0014"},
            {"prefixe": "898", "section": "BC", "numero": "0016"},
            {"prefixe": "898", "section": "H", "numero": "15"},
        ],
    },
    "demandeurs": [
        {
            "type_personne": "particulier",
            "typologie": "petitionnaire",
            "nom": "Pierre",
            "prenom": "Martin",
            "adresse": {
                "numero_voie": "28b",
                "nom_voie": "Boulevard Liberté",
                "lieu_dit": "L'Estaque'",
                "code_postal": 13016,
                "localite": "Marseille",
            },
            "coordonnees": {"email": "p.martin@blackmail.com"},
        }
    ],
}


class BaseTestCase(unittest.TestCase):
    """."""

    def setUp(self):
        """."""
        self.current_path = os.path.dirname(os.path.abspath(__file__))
        self.app = TestApp(get_app(f"{self.current_path}/resources/dev.ini"))
        # self.app.authorization = ("Basic", ("new_orleans", "secret"))
        self.app.authorization = ("Basic", ("aubagne", "secret"))

    def add_instruction(self, dossier_id, instruction_type, finalize=False):
        """Helper methods.

        Allow you to add a straight instruction on a Dossier d'Instruction
        """
        # Get backend config from config test
        backend = self.load_config()["backends"]["dev"]

        # We have to unwrapped some well-wrapped presents
        backend["password"] = unbox_surprise(backend["password"])

        omobj = OMBaseObject(backend)
        data = {
            "dossier": dossier_id,
            "evenement": instruction_type,
            "date_evenement": date.today().strftime("%d/%m/%Y"),
        }

        html = omobj._post(
            "instruction",
            data,
            module="sousform",
            url_vars={"idxformulaire": dossier_id},
        )
        instruction_id = drop_linebreak(
            html.select(".form-content #instruction")[0].attrs["value"]
        )

        if not finalize:
            return
        # We finalize the courrier
        omobj._post(
            "instruction",
            None,
            module="sousform",
            action=100,
            url_vars={"idx": instruction_id, "idxformulaire": dossier_id},
        )

    def load_config(self):
        """."""
        config_path = (
            os.path.dirname(os.path.abspath(__file__)) + "/resources/config.json"
        )
        with open(config_path, "r") as f:
            return json.load(f)


def drop_linebreak(value):
    """."""
    if value.endswith("\n"):
        return value[:-1]


def get_file_data(path, b64=True):
    """."""
    with open(path, 'rb') as f:
        if b64:
            return str(b64encode(f.read()), 'utf-8')
        return str(f.read(), 'utf-8')
