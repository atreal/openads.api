# -*- coding: utf-8 -*-

import os

from .base_testcase import BaseTestCase, get_file_data, dossier_payload
from datetime import datetime, timedelta


class FilesTestCase(BaseTestCase):
    """."""

    def test_add_pieces(self):
        """."""
        self.app.authorization = ("Basic", ("aubagne", "secret"))
        """."""

        base_payload = dossier_payload.copy()

        # We set a creation date in the past. Used to validate the reception date as IRL
        base_payload["date_demande"] = (datetime.today() - timedelta(days=10)).strftime(
            "%d/%m/%Y"
        )

        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", base_payload, status=200)

        # And get the numero
        numero_dossier = response.json["numero_dossier"]

        # Then prep some nice PDF
        pdf_dir = os.path.dirname(os.path.abspath(__file__)) + "/resources"
        dia_b64 = get_file_data("%s/DIA_cerfa_10072-02.pdf" % pdf_dir)
        plan_b64 = get_file_data("%s/plancadastral.pdf" % pdf_dir)

        # And the right payload
        pieces_payload = [
            {
                "filename": "DIA_cerfa_10072-02.pdf",
                "content_type": "application/pdf",
                "b64_content": dia_b64,
                "piece_type": 1,
            },
            {
                "filename": "plancadastral.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 5,
            },
        ]

        # Let's add some file !
        response = self.app.post_json(
            "/dossier/%s/pieces" % numero_dossier, pieces_payload, status=200
        )

        # XXX: here, we should test if the recepion date set within the Piece matches
        # the one provided in the dossier payload

        pieces_created = response.json["pieces"]
        for piece in pieces_created:
            self.assertEqual(piece["status"], "OK")
        filenames = [p["filename"] for p in pieces_payload]
        for item in pieces_payload:
            self.assertTrue(item["filename"] in filenames)

        # Let's try the special 111 demande type usecase
        # And the right payload
        pieces_payload = [
            {
                "filename": "bad_typing.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 111,
            },
        ]
        # Let's add some file !
        # This should break with a 400
        response = self.app.post_json(
            "/dossier/%s/pieces" % numero_dossier, pieces_payload, status=400
        )

        pieces_payload = [
            {
                "filename": "good_typing.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 111,
                "custom_piece_type": "CU précédent",
            },
        ]
        # Let's add some file !
        response = self.app.post_json(
            "/dossier/%s/pieces" % numero_dossier, pieces_payload, status=200
        )
        pieces_created = response.json["pieces"]
        self.assertEqual(len(pieces_created), 1)

    def test_stressed_add_pieces(self):
        """."""
        self.app.authorization = ("Basic", ("aubagne", "secret"))

        # Let's create a simple dossier
        response = self.app.post_json("/dossiers", dossier_payload, status=200)

        # And get the numero
        numero_dossier = response.json["numero_dossier"]

        # Then prep some nice PDF
        pdf_dir = os.path.dirname(os.path.abspath(__file__)) + "/resources"
        dia_b64 = get_file_data("%s/DIA_cerfa_10072-02.pdf" % pdf_dir)
        plan_b64 = get_file_data("%s/plancadastral.pdf" % pdf_dir)

        # And the right payload
        payload = [
            {
                "filename": "DIA_cerfa_10072-02.pdf",
                "content_type": "application/pdf",
                "b64_content": dia_b64,
                "piece_type": 1,
            },
            {
                "filename": "plancadastral.pdf",
                "content_type": "application/pdf",
                "b64_content": plan_b64,
                "piece_type": 5,
            },
        ]
        for i in range(5):
            payload.extend(payload)

        # Let's add some file !
        response = self.app.post_json(
            "/dossier/%s/pieces" % numero_dossier, payload, status=200
        )

        pieces_created = response.json["pieces"]
        for piece in pieces_created:
            self.assertEqual(piece["status"], "OK")
        payload_filenames = [p["filename"] for p in payload]
        for item in payload:
            self.assertTrue(item["filename"] in payload_filenames)
