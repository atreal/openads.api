# -*- coding: utf-8 -*-

from .base_testcase import BaseTestCase, COLLECTIVITE_INSEE


class OMTestCase(BaseTestCase):
    """."""

    def setUp(self):
        """."""
        super().setUp()

    def test_get_config(self):
        """."""
        # Let's get some non-existing om_parametre
        response = self.app.get("/config/im_everything_but_a_parameter", status=200)

        # Let's get some om_parametre
        response = self.app.get(
            "/config/option_notification_piece_numerisee", status=200
        )
        # Is the structure as expected ?
        expected_keys = {"label", "value", "collectivite"}
        for prop in response.json:
            self.assertEquals(set(prop.keys()), expected_keys)

        self.assertIn(
            "option_notification_piece_numerisee", [p["label"] for p in response.json]
        )
        for prop in response.json:
            if prop["label"] == "option_notification_piece_numerisee":
                self.assertIn(prop["value"], ("true", "false"))

        # Let's try the multiple parameters response
        response = self.app.get("/config/option_*", status=200)
        self.assertGreater(len(response.json), 1)

    def test_get_filtered_config(self):
        """."""
        # Let's get some non-existing om_parametre
        response = self.app.get(
            f"/config/im_everything_but_a_parameter?insee={COLLECTIVITE_INSEE}",
            status=200,
        )
        self.assertEquals(response.json, [])

        # Let's get some valid om_parametre, but non-existant insee
        response = self.app.get(
            "/config/maire?insee=99999",
            status=200,
        )
        self.assertEquals(response.json, [])

        # Let's get some om_parametre
        response = self.app.get(
            f"/config/maire?insee={COLLECTIVITE_INSEE}",
            status=200,
        )
        # We should have one and only one param
        self.assertEqual(len(response.json), 1)
        # Is the structure as expected ?
        expected_keys = {"label", "value", "collectivite"}
        for prop in response.json:
            self.assertEquals(set(prop.keys()), expected_keys)

        self.assertIn(
            "maire", [p["label"] for p in response.json]
        )

        # Let's get some om_parametre, with a publik renderer
        response = self.app.get(
            f"/config/maire?insee={COLLECTIVITE_INSEE}&response_format=publik",
            status=200,
        )
        # Response should match Publik format
        expected_keys = {"err", "data"}
        self.assertEquals(set(response.json.keys()), expected_keys)

        # Is the structure as expected ?
        expected_keys = {"label", "value", "collectivite"}
        for prop in response.json["data"]:
            self.assertEquals(set(prop.keys()), expected_keys)
