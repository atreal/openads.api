from cornice import Service
from cornice.service import get_services
from cornice_swagger import CorniceSwagger
from openads.api import PACKAGE_VERSION

# Create a service to serve our OpenAPI spec
swagger = Service(name="OpenAPI", path="/__api__", description="OpenAPI documentation")


@swagger.get()
def openAPI_spec(request):
    doc = CorniceSwagger(get_services())

    # Extra parameters that should be merged with the extracted info from the generate call.
    doc.swagger = {
        "schemes": ["https"],
        "securityDefinitions": {"basicAuth": {"type": "basic"}},
        "security": [{"basicAuth": []}],
    }

    # Enable extracting operation summaries from view docstrings.
    # doc.summary_docstrings = True

    return doc.generate("openADS API", PACKAGE_VERSION)
