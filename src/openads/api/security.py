from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.security import forget
from pyramid.view import forbidden_view_config
from passlib.hash import sha512_crypt

import logging

log = logging.getLogger(__name__)


@forbidden_view_config()
def forbidden_view(request):
    if request.authenticated_userid is None:
        response = HTTPUnauthorized()
        response.headers.update(forget(request))

    # user is logged in but doesn't have permissions, reject wholesale
    else:
        response = HTTPForbidden()
    return response


def check_credentials(username, password, request):
    """."""
    conf = request.registry.settings["openads.api.config"]
    credentials_storage = request.registry.settings["openads.api.credentials_storage"]

    try:
        info = conf["users"][username]
    except KeyError:
        # username unkown
        log.debug("user %s not found in our base" % username)
        return

    if (
        credentials_storage == "hashed"
        and sha512_crypt.verify(
            password.encode("utf-8"), info["password"].encode("utf-8")
        )
    ) or (
        credentials_storage == "plain"
        and password.encode("utf-8") == info["password"].encode("utf-8")
    ):
        log.debug("user %s authenticated" % username)
        return []
    else:
        log.debug("password for user %s doesn't match" % username)
        return
