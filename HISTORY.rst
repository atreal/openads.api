*******************************
openads.api history / changelog
*******************************

1.3.3 (unreleased)
==================

- Nothing changed yet.


1.3.2 (2025-03-14)
==================

Features
--------

* More flexibility in while parsing Dossier number
  Mandatory to cover new usecases (DIA, APE, DPE, etc)

* Support "canceled" value as state search criterion
  See https://gitlab.com/atreal/openads.api/-/issues/#14


1.3.1 (2023-10-06)
==================

Fixes
-----

* openmairie.ws: params might be retrieved by insee value
  See https://gitlab.com/atreal/openads.platau/-/issues/90

* openads.api: support any config filename with .json


1.3.0 (2023-03-15)
==================

Features
--------

* Log called route + url_vars

* Provide /pieces WS
  Related to open-capture

* Unified PACKAGE_VERSION
  PACKAGE_VERSION is now built in __init__ and imported when needed

* Support Donnees Techniques in legacy WS
  Mainly necessary for building test dataset


1.2.0 (2023-01-06)
==================

Features
--------

* openmairie.ws: support download file without extension
  https://gitlab.com/atreal/openads.api/-/issues/11

* openmairie.ws: provide mandatory query value on `_update`

* openmairie.ws: drop useless query var in `_set_file`


Fixes
-----

* openads.ws: drop useless `post` var that might cause issue


1.1.2 (2022-12-06)
==================

Fixes
-----

* correct openmairie.ws version


1.1.1 (2022-12-06)
==================

Fixes
-----

* support mono CT while retrieving config
  https://gitlab.com/atreal/openads.api/-/issues/10


1.1.0 (2022-12-02)
==================

Features
--------

* Support config with insee code filtering

* Refactor Operation endpoints

* New `/pieces` enpoint


1.0.0 (2022-11-03)
==================

Features
--------

* Add a HISTORY.rst !

* Move old matser branch to legacy one

* Refactor Piece WS
  In order to expose publicly /dossier/{id}/pieces:
    + properly document schemas
    + add Piece endpoint

* tests: finalize & sign Instruction

* Refactor Dossier operations:
    + move views and schema to separates files
    + introduce response  scheam
    + properly document endpoint schemas

Fixes
-----

*  Make the tests altered by plain storage psswd working again.
  Following new feature 'password storage as plain text', tests were failing in case of
  mixed strategy.
  Also :
    - Standalone tests are now launched with dev.ini config:
      `self.app = TestApp(get_app(f"{self.current_path}/resources/dev.ini")`
    - When running test with mixed passwords strategy, we ignore the failing
    entries



